class Card {
  constructor(type) {
    if (!(type === 'narrow' || type === 'wide')) {
      throw new Error('указан неверный тип карточки');
    }
    this.type = type;
    this._template = document.querySelector('template').content.querySelector('.card') || showError('Шаблон карточки не найден');
    this.node = this._template.cloneNode(true);
    this.node.classList.add(`card--${this.type}`);
  }

  render(parentNode) {
    parentNode.appendChild(this.node);
  }
}

function showError(msg) {
  var errorBox = document.createElement('div');
  errorBox.classList.add('error-box');
  errorBox.textContent = msg || 'Произошла ошибка.';
  document.body.insertAdjacentElement('afterbegin', errorBox);
  errorBox.addEventListener('click', () => {
    errorBox.remove();
  });
  throw new Error(msg);
}

document.addEventListener('DOMContentLoaded', () => {
  const containerForCards = document.querySelector('.cards-list');
  let listCards = [];

  /**
   * Записывает типы отрисованных карточек в window.history
   *
   * @param {Array} arrayCards - массив карточек отображаемых на странице
   */
  function writePageStateToHistory(arrayCards) {
    const pageState = arrayCards.map((item)=> {
      return {type: item.type};
    });
    window.history.pushState(pageState, null, null);
  }

  /**
   * Создает и отрисовывает карточки на странице, если данных нет выводит ошибку
   *
   * @param {Object} inputData - входящие данные
   * @param {Node} container - контейнер для карточек
   */
  function renderCards(inputData, container) {
    if (typeof inputData !== 'undefined' && inputData) {
      const fragment = document.createDocumentFragment();
      inputData.forEach((item) => {
        listCards.push(new Card(item.type));
      });
      listCards.forEach((card) => {
        fragment.appendChild(card.node);
      });
      container.appendChild(fragment);
    } else {
      showError('Данные о карточках недоступны');
    }
  }

  renderCards(window.cards, containerForCards);
  writePageStateToHistory(listCards);

  containerForCards.addEventListener('mouseover', (evt) => {
    if (evt.path[0].classList.contains('cards-list')) {
      containerForCards.classList.toggle('overlay', true);
    } else {
      containerForCards.classList.toggle('overlay', false);
    }
  });

  containerForCards.addEventListener('click', (evt) => {
    const card = evt.target.closest('.card');
    if (!card) return;
    if (!containerForCards.contains(card)) return;

    if (evt.shiftKey) {
      let newCard;
      newCard = new Card(evt.altKey ? 'wide' : 'narrow');
      newCard.render(containerForCards);
      listCards.push(newCard);
    } else {
      const lastCard = listCards.pop();
      lastCard.node.remove();
    }
    writePageStateToHistory(listCards);
  });

  window.addEventListener('popstate', () => {
    if (window.history.state) {
      containerForCards.innerHTML = '';
      listCards.length = [];
      renderCards(window.history.state, containerForCards);
    }
  });
});
